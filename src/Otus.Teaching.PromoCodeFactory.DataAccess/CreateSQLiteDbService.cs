﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public interface ICreateSqLiteDbService
    {
        void InitDb();
    }
    public class CreateSqLiteDbService : ICreateSqLiteDbService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        public CreateSqLiteDbService(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory= serviceScopeFactory;
        }

        public void InitDb()
        {
            using var scope = _serviceScopeFactory.CreateScope();
            
            var myDbContext = scope.ServiceProvider.GetService<SqliteDbContext>();
            if (myDbContext == null) return;
            myDbContext.Database.EnsureDeleted();
            myDbContext.Database.EnsureCreated();
            
            myDbContext.Roles.AddRange(FakeDataFactory.Roles);
            myDbContext.Employees.AddRange(FakeDataFactory.Employees);
            
            
            
            myDbContext.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            
            myDbContext.Customers.AddRange(FakeDataFactory.Customers);
            myDbContext.Preferences.AddRange(FakeDataFactory.Preferences);
            
            FakeDataFactory.CreateCustomerPreferences(FakeDataFactory.Customers.ElementAt(0),
                FakeDataFactory.Preferences.ElementAt(0));

            foreach (var preference in FakeDataFactory.Preferences)
            {
                FakeDataFactory.CreateCustomerPreferences(FakeDataFactory.Customers.ElementAt(1),preference);
            }
            
            
            //myDbContext.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);
            
            myDbContext.SaveChanges();
        }
    }
}
