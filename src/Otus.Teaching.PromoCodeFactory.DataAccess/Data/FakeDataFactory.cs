﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static IEnumerable<Employee>? _employees;
        public static IEnumerable<Employee> Employees 
        {
            get
            {
                return _employees ??= new List<Employee>()
                {
                    new()
                    {
                        Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                        Email = "owner@somemail.ru",
                        FirstName = "Иван",
                        LastName = "Сергеев",
                        Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                        AppliedPromocodesCount = 5
                    },
                    new()
                    {
                        Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                        Email = "andreev@somemail.ru",
                        FirstName = "Петр",
                        LastName = "Андреев",
                        Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                        AppliedPromocodesCount = 10
                    }
                };
            }
        }

        private static IEnumerable<Role>? _roles;
        public static IEnumerable<Role> Roles
        {
            get
            {
                return _roles ??= new List<Role>()
                {
                    new()
                    {
                        Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                        Name = "Admin",
                        Description = "Администратор",
                    },
                    new()
                    {
                        Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                        Name = "PartnerManager",
                        Description = "Партнерский менеджер"
                    }
                };
            }
        }

        private static IEnumerable<Preference>? _preferences;
        public static IEnumerable<Preference> Preferences
        {
            get
            {
                return _preferences ??= new List<Preference>()
                {
                    new()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                        Name = "Театр",
                    },
                    new()
                    {
                        Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                        Name = "Семья",
                    },
                    new()
                    {
                        Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                        Name = "Дети",
                    }
                };
            }
        } 
            

        private static IEnumerable<PromoCode>? _promoCodes;
        public static IEnumerable<PromoCode> PromoCodes 
        { 
            get 
            {                
                return _promoCodes ??= new List<PromoCode>()
                {
                    new()
                    {
                        Id = Guid.NewGuid(),
                        Code = "TRASH",
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(4),
                        PartnerManager = Employees.ElementAt(0),
                        PartnerName = "CSBI RPA",
                        Preference = Preferences.ElementAt(0)
                    },
                    new()
                    {
                    Id = Guid.NewGuid(),
                    Code = "New week",
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(4),
                    PartnerManager = Employees.ElementAt(0),
                    PartnerName = "WB",
                    Preference = Preferences.ElementAt(0)
                    }
                }; 
            } 
        }
        
        

        private static IEnumerable<Customer>? _customers;
        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customer1 = new Customer()
                {
                    Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                    Email = "ivan_sergeev@mail.ru",
                    FirstName = "Иван",
                    LastName = "Петров",
                    PromoCodes = new List<PromoCode>()
                    {
                        PromoCodes.ElementAt(0)
                    }
                };
                
                var customer2 = new Customer()
                {
                    Id = Guid.Parse("11c8c6b1-1111-1111-1111-244740aaf011"),
                    Email = "otas@mail.ru",
                    FirstName = "Атас",
                    LastName = "Атасович",
                    PromoCodes = new List<PromoCode>()
                    {
                        PromoCodes.ElementAt(0)
                    }
                };

                return _customers ??= new List<Customer>()
                {
                    customer1, 
                    customer2
                };
            }
        }

        public static void CreateCustomerPreferences(Customer customer, Preference preference)
        {
            customer.Preferences ??= new();
            customer.Preferences.Add(preference);
            
            preference.Customers ??= new();
            preference.Customers.Add(customer);
        }
    }
}