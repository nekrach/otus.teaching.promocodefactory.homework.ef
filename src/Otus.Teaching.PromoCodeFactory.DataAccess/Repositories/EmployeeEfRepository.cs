﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EmployeeEfRepository : IRepository<Employee>
{
    private readonly SqliteDbContext _dbContext;

    public EmployeeEfRepository(SqliteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    async Task<IEnumerable<Employee>> IRepository<Employee>.GetAllAsync()
    {
        return await _dbContext.Employees
            .AsNoTracking()
            .ToListAsync();
    }

    async Task<Employee> IRepository<Employee>.GetByIdAsync(Guid id)
    {
        return await _dbContext.Employees
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    async Task<bool> IRepository<Employee>.DeleteByIdAsync(Guid id)
    {
        var entity = await _dbContext.Employees.FirstOrDefaultAsync(x => x.Id == id);
        if (entity is { })
        {
            _dbContext.Employees.Remove(entity);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    async Task<Guid> IRepository<Employee>.CreateAsync(Employee newItem)
    {
        var entity = await _dbContext.Set<Employee>().AddAsync(newItem);
        await _dbContext.SaveChangesAsync();
        return  entity.Entity.Id;
    }

    async Task<bool> IRepository<Employee>.UpdateAsync(Employee newItem)
    {
        var entity = _dbContext.Employees.Update(newItem);
        if(entity is {})
        {
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }
}