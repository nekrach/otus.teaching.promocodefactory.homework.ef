﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class CustomerEfRepository : IRepository<Customer>
{
    private readonly SqliteDbContext _dbContext;

    public CustomerEfRepository(SqliteDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    async Task<IEnumerable<Customer>> IRepository<Customer>.GetAllAsync()
    {
        return await _dbContext.Customers
            .AsNoTracking()
            .Include(c=>c.Preferences)
            .Include(c=>c.PromoCodes)
            .ToListAsync();
    }

    async Task<Customer> IRepository<Customer>.GetByIdAsync(Guid id)
    {
        return await _dbContext.Customers
            .AsNoTracking()
            .Include(c=>c.Preferences)
            .Include(c=>c.PromoCodes)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    async Task<bool> IRepository<Customer>.DeleteByIdAsync(Guid id)
    {
        var entity = await _dbContext.Customers
            .Include(c=>c.PromoCodes)
            .FirstOrDefaultAsync(x => x.Id == id);
        if (entity is { })
        {
            entity.PromoCodes?.Clear();
            _dbContext.Customers.Remove(entity);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    async Task<Guid> IRepository<Customer>.CreateAsync(Customer newItem)
    {
        var entity = await _dbContext.Customers.AddAsync(newItem);
        return  entity.Entity.Id;
    }

    async Task<bool> IRepository<Customer>.UpdateAsync(Customer newItem)
    {
        var updatedCustomer = _dbContext.Customers
            .Include(c=>c.Preferences)
            .Include(c=>c.PromoCodes)
            .FirstOrDefault(x => x.Id == newItem.Id);

        if(updatedCustomer is {})
        {
            updatedCustomer.FirstName = newItem.FirstName;
            updatedCustomer.LastName = newItem.LastName;
            updatedCustomer.Email = newItem.Email;
            updatedCustomer.Preferences = newItem.Preferences;
            updatedCustomer.PromoCodes = newItem.PromoCodes;
            await _dbContext.SaveChangesAsync();
            return true;
        }     

        return false;

    }
}