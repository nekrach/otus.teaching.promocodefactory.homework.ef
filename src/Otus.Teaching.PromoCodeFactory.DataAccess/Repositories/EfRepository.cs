﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly SqliteDbContext _dbContext;

        public EfRepository(SqliteDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dbContext.Set<T>().ToListAsync();
            return entities;
        }
        
        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            var entity = await _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if(entity is{})
            {
                var deleteEntity = _dbContext.Set<T>().Remove(entity);
                if (deleteEntity is { })
                {
                    await _dbContext.SaveChangesAsync();
                    return true;
                }
            }

            return false;
        }

        public async Task<Guid> CreateAsync(T newItem)
        {
            var addEntity = await _dbContext.Set<T>().AddAsync(newItem);
            if(addEntity is{})
            {
                await _dbContext.SaveChangesAsync();
                return addEntity.Entity.Id;
            }

            return Guid.Empty;
        }

        public async Task<bool> UpdateAsync(T newItem)
        {
            var updateEntity =  _dbContext.Set<T>().Update(newItem);
            if(updateEntity is{})
            {
                await _dbContext.SaveChangesAsync();
                return true;
            }

            return false;
        }

        
        
        
    }
}
