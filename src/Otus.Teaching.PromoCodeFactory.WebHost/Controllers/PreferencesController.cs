﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class PreferencesController
    : ControllerBase
{
    private readonly IRepository<Preference> _efRepositoryPreference;

    public PreferencesController(IRepository<Preference> efRepositoryPreference)
    {
        _efRepositoryPreference = efRepositoryPreference;
    }
    
    /// <summary>
    /// Получение списка предпочтений
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
    {
        var preferences = await _efRepositoryPreference.GetAllAsync();
        
        var result = preferences.ToList().Select(preference =>
            new PreferenceResponse
            {
                Id = preference.Id,
                Name = preference.Name!
            }).ToList();
            
        return Ok(result);
    }
}