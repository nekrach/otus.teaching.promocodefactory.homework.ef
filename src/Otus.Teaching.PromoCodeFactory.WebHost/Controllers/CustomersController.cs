﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _efRepositoryCustomer;
        private readonly IRepository<Preference> _efRepositoryPreference;

        public CustomersController(IRepository<Customer> efRepositoryCustomer, 
            IRepository<Preference> efRepositoryPreference)
        {
            _efRepositoryCustomer = efRepositoryCustomer;
            _efRepositoryPreference = efRepositoryPreference;
        }

        /// <summary>
        /// получение списка клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            // Добавить получение списка клиентов
            var customers = await _efRepositoryCustomer.GetAllAsync();
            var result = customers.ToList().Select(customer =>
                new CustomerResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName!,
                    LastName = customer.LastName!,
                    Email = customer.Email!,
                    PromoCodes = customer.PromoCodes?
                        .Select(code => new PromoCodeShortResponse
                        {
                            Id = code.Id,
                            Code = code.Code!,
                            ServiceInfo = code.ServiceInfo!,
                            BeginDate = code.BeginDate.ToString(CultureInfo.InvariantCulture),
                            EndDate = code.EndDate.ToString(CultureInfo.InvariantCulture),
                            PartnerName = code.PartnerName!
                        }).ToList()!,
                    Preferences = customer.Preferences?
                        .Select(preference => new PreferenceResponse
                        {
                            Name = preference.Name!
                        }).ToList()
                }).ToList();
            
            return Ok(result);
        }
        
        /// <summary>
        /// получение клиента вместе с выданными ему промомкодами
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            // Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _efRepositoryCustomer.GetByIdAsync(id);
            
            var result = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName!,
                LastName = customer.LastName!,
                Email = customer.Email!,
                PromoCodes = customer.PromoCodes?
                    .Select(code => new PromoCodeShortResponse
                    {
                        Id = code.Id,
                        Code = code.Code!,
                        ServiceInfo = code.ServiceInfo!,
                        BeginDate = code.BeginDate.ToString(CultureInfo.InvariantCulture),
                        EndDate = code.EndDate.ToString(CultureInfo.InvariantCulture),
                        PartnerName = code.PartnerName!
                    }).ToList()!,
                Preferences = customer.Preferences?
                    .Select(preference => new PreferenceResponse
                    {
                        Id = preference.Id,
                        Name = preference.Name!
                    }).ToList()
            };

            return Ok(result);
        }
        
        /// <summary>
        /// создание нового клиента вместе с его предпочтениями
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            // Добавить создание нового клиента вместе с его предпочтениями
            var preferences = (await _efRepositoryPreference.GetAllAsync())
                .Where(x =>
                    request.PreferenceIds.Any(y => y.Equals(x.Id))).ToList();
            
            var createEntry = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences
            };
            var result = await _efRepositoryCustomer.CreateAsync(createEntry);
            return Ok(result);
        }
        
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            // Обновить данные клиента вместе с его предпочтениями
            var customer = await _efRepositoryCustomer.GetByIdAsync(id);

            if (customer == null)
                return BadRequest();
            
            var preferences = (await _efRepositoryPreference.GetAllAsync())
                .Where(x =>
                    request.PreferenceIds.Any(y => y.Equals(x.Id))).ToList();

            var updateCustomer = new Customer()
            {
                Id = customer.Id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = preferences,
            };

            return Ok(await _efRepositoryCustomer.UpdateAsync(updateCustomer));
        }
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //Удаление клиента вместе с выданными ему промокодами
            var result = await _efRepositoryCustomer.DeleteByIdAsync(id);
            if(result)
                return Ok();

            return BadRequest();
        }
    }
}